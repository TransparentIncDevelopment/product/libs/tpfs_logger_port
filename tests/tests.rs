//! Unfortunately this test needs its own crate because the
//! macro can't be used in its
//! own
//! crate
//! and the trait can't be implemented with an absolute path in *its* own crate.
#![forbid(unsafe_code)]
#![allow(clippy::unwrap_used)]

use log::{Level, Metadata, Record};
use serde_derive::Serialize;
use std::{
    fmt::Debug,
    sync::atomic::{AtomicUsize, Ordering},
    sync::mpsc::{sync_channel, SyncSender},
};
use tpfs_logger_port::{
    add_log_observer, debug, error, info, logging_event, trace, warn, EventObserver, LogKVPairName,
};

// Tests for macros are fun. Half the test is just that it compiles!
#[logging_event]
#[derive(Serialize)]
enum AllTypesOfVariants {
    Unit,
    OneUnNamed(bool),
    MultiUnNamed(bool, bool, bool),
    Named { _thing: bool },
    MultiNamed { _thing: bool, _thing2: bool },
}

#[test]
fn macro_names_work() {
    assert_eq!(AllTypesOfVariants::Unit.name(), "Unit");
    assert_eq!(AllTypesOfVariants::OneUnNamed(true).name(), "OneUnNamed");
    assert_eq!(
        AllTypesOfVariants::MultiUnNamed(true, true, true).name(),
        "MultiUnNamed"
    );
    assert_eq!(AllTypesOfVariants::Named { _thing: false }.name(), "Named");
    assert_eq!(
        AllTypesOfVariants::MultiNamed {
            _thing: true,
            _thing2: false
        }
        .name(),
        "MultiNamed"
    );
}

#[logging_event]
#[derive(Serialize, Eq, PartialEq)]
enum TestEvents {
    ThingOne,
    MoreComplex { thing: String },
}

// This is a janky test fake, for sure, but because `Record`s are !Send and !Sync but `Log`
// trait is required to be Send and Sync, doing this in a nice way is incredibly difficult so
// - the test below is the only test that should use this struct, and the assertions in the
// struct must be in sync with the test's logging
struct FakeLogger {
    assert_count: AtomicUsize,
}
impl log::Log for FakeLogger {
    fn enabled(&self, _: &Metadata<'_>) -> bool {
        true
    }

    fn log(&self, record: &Record<'_>) {
        match self.assert_count.load(Ordering::SeqCst) {
            // Unfortunately these can't be de-duped as `format_args!` has to be used inline.
            0 => compare_recs(
                record,
                &Record::builder()
                    .level(Level::Trace)
                    .target(module_path!())
                    .args(format_args!("{}", "{\"evt\":\"ThingOne\",\"val\":null}"))
                    .build(),
            ),
            1 => compare_recs(
                record,
                &Record::builder()
                    .level(Level::Debug)
                    .target(module_path!())
                    .args(format_args!("{}", "{\"evt\":\"ThingOne\",\"val\":null}"))
                    .build(),
            ),
            2 => compare_recs(
                record,
                &Record::builder()
                    .level(Level::Info)
                    .target(module_path!())
                    .args(format_args!(
                        "{}",
                        "{\"evt\":\"MoreComplex\",\"val\":{\"thing\":\"hi!\"}}"
                    ))
                    .build(),
            ),
            3 => compare_recs(
                record,
                &Record::builder()
                    .level(Level::Warn)
                    .target(module_path!())
                    .args(format_args!("{}", "{\"evt\":\"ThingOne\",\"val\":null}"))
                    .build(),
            ),
            4 => compare_recs(
                record,
                &Record::builder()
                    .level(Level::Error)
                    .target(module_path!())
                    .args(format_args!("{}", "{\"evt\":\"ThingOne\",\"val\":null}"))
                    .build(),
            ),
            _ => panic!("Too much logging! Not enough assertions!"),
        }
        self.assert_count.fetch_add(1, Ordering::SeqCst);
    }

    fn flush(&self) {}
}

// Ugh, records are not comparable
fn compare_recs(r1: &Record<'_>, r2: &Record<'_>) {
    assert_eq!(r1.level(), r2.level());
    assert_eq!(r1.target(), r2.target());
    assert_eq!(format!("{}", r1.args()), format!("{}", r2.args()))
}

#[test]
fn test_static_logging() {
    // Since this sets the global static logger, this test encompasses a few things
    log::set_boxed_logger(Box::new(FakeLogger {
        assert_count: AtomicUsize::new(0),
    }))
    .unwrap();

    // Set up an event observer
    let (tx, rx) = sync_channel::<String>(10);
    struct FakeObserver(pub SyncSender<String>);

    impl EventObserver for FakeObserver {
        fn observe(&self, event_name: &'static str, _full_record: &Record) {
            self.0.send(event_name.to_string()).unwrap();
        }
    }

    add_log_observer(FakeObserver(tx));

    // Log some events that match up with the expectations in the fake logger
    trace!(TestEvents::ThingOne);
    debug!(TestEvents::ThingOne);
    info!(TestEvents::MoreComplex {
        thing: "hi!".to_string()
    });
    warn!(TestEvents::ThingOne);
    error!(TestEvents::ThingOne);
    // Verify observer saw the events
    assert_eq!(rx.try_recv().unwrap(), TestEvents::ThingOne.name());
    assert_eq!(rx.try_recv().unwrap(), TestEvents::ThingOne.name());
    assert_eq!(
        rx.try_recv().unwrap(),
        TestEvents::MoreComplex {
            thing: "sf".to_string()
        }
        .name()
    );
}
