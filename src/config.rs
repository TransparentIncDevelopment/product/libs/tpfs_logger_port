use crate::{HUMAN_READABLE_ENV_VAR, LOG_LEVEL_ENV_VAR, LOG_TARGET_LEVEL_ENV_VAR};
use log::LevelFilter;
use std::{collections::HashMap, env};

pub struct TpfsLogCfg {
    /// The default logging level that all targets, unless overridden, will use.
    pub default_level: LevelFilter,
    /// Allows setting specific levels for certain logging targets.
    pub target_to_level: HashMap<String, LevelFilter>,
}

impl TpfsLogCfg {
    #[must_use]
    pub fn human_readable_mode() -> bool {
        env::var(HUMAN_READABLE_ENV_VAR).is_ok()
    }
}

impl Default for TpfsLogCfg {
    #[must_use]
    fn default() -> Self {
        let default_level =
            env::var(LOG_LEVEL_ENV_VAR).map_or(LevelFilter::Info, |val| str_to_loglevel(&val));
        let target_to_level = env::var(LOG_TARGET_LEVEL_ENV_VAR).map_or_else(
            |_| HashMap::new(),
            |val| {
                let mut map = HashMap::new();

                let filters = val.split(';');

                for filterspec in filters {
                    let split: Vec<_> = filterspec.split('=').collect();
                    if split.len() != 2 {
                        eprintln!("Invalid logging filter spec '{}', ignoring.", filterspec);
                        continue;
                    }
                    map.insert(split[0].to_string(), str_to_loglevel(split[1]));
                }

                map
            },
        );
        Self {
            default_level,
            target_to_level,
        }
    }
}

fn str_to_loglevel(val: &str) -> LevelFilter {
    match val.to_uppercase().as_ref() {
        "ERROR" => LevelFilter::Error,
        "WARN" => LevelFilter::Warn,
        "DEBUG" => LevelFilter::Debug,
        "TRACE" => LevelFilter::Trace,
        "OFF" => LevelFilter::Off,
        _ => LevelFilter::Info,
    }
}
